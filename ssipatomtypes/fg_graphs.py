import networkx as nx

# If the SSIP atom type is the same as the sybyl atom type, 
#you should indicate the ssipAtomType here as None

#HYDROGEN

nh = nx.Graph()
nh.add_node("N", elementType="N", ssipAtomType=None)
nh.add_node("H", elementType="H", ssipAtomType="H.N" )
nh.add_edge("N", "H")

oh = nx.Graph()
oh.add_node("O", elementType="O", ssipAtomType=None)
oh.add_node("H", elementType="H", ssipAtomType="H.O" )
oh.add_edge("O", "H")

softh = nx.Graph()
softh.add_node("O", elementType=["!N", "!O"], ssipAtomType=None)
softh.add_node("H", elementType="H", ssipAtomType="H.soft" )
softh.add_edge("O", "H")

#OXYGEN AND NITROGEN

amide = nx.Graph()
amide.add_node("C", elementType="C", ssipAtomType=None)
amide.add_node("O", elementType="O", ssipAtomType="O.2.am" )
amide.add_node("N", elementType="N", ssipAtomType="N.am" )
amide.add_edge("C", "O" , bondOrder=2)
amide.add_edge("C", "N")

nitro = nx.Graph()
nitro.add_node("O1", elementType="O", ssipAtomType="O.2.nitro")
nitro.add_node("N", elementType="N", ssipAtomType="N.pl3.nitro" )
nitro.add_node("O2", elementType="O", ssipAtomType="O.2.nitro")
nitro.add_edge("O1", "N" , bondOrder=2)
nitro.add_edge("O2", "N" , bondOrder=1)

#OXYGEN

water = nx.Graph()
water.add_node("H1", elementType="H", ssipAtomType=None)
water.add_node("O", elementType="O", ssipAtomType="O.3.water" )
water.add_node("H2", elementType="H", ssipAtomType=None)
water.add_edge("H1", "O" , bondOrder=1)
water.add_edge("H2", "O" , bondOrder=1)


#The carbonyl graph needs to be matched after the amide graph
carbonyl = nx.Graph()
carbonyl.add_node("C", elementType="C", ssipAtomType=None)
carbonyl.add_node("O", elementType="O", ssipAtomType="O.2.carbonyl" )
carbonyl.add_edge("C", "O" , bondOrder=2)

po = nx.Graph()
po.add_node("P", elementType="P", ssipAtomType=None)
po.add_node("O", elementType="O", ssipAtomType="O.2.po" )
po.add_edge("P", "O", bondOrder=2 )

alcohol = nx.Graph()
alcohol.add_node("H1", sybyl="H", ssipAtomType=None)
alcohol.add_node("O", sybyl="O.3", ssipAtomType="O.3.alcohol" )
alcohol.add_node("C", sybyl="C.3", ssipAtomType=None)
alcohol.add_edge("H1", "O" , bondOrder=1)
alcohol.add_edge("C", "O" , bondOrder=1)

#The any O3 needs to be matched after the alcohol
any_O3 = nx.Graph()
any_O3.add_node("A1", ssipAtomType=None)
any_O3.add_node("O", elementType="O", ssipAtomType="O.3.any" )
any_O3.add_node("A2", ssipAtomType=None)
any_O3.add_edge("A1", "O", bondOrder=1)
any_O3.add_edge("A2", "O", bondOrder=1)

sulfone = nx.Graph()
sulfone.add_node("S", sybyl="S.O2", ssipAtomType=None)
sulfone.add_node("O1", sybyl="O.2", ssipAtomType="O.2.sulfone" )
sulfone.add_node("O2", sybyl="O.2", ssipAtomType="O.2.sulfone" )
sulfone.add_edge("S", "O1" )
sulfone.add_edge("S", "O2")

sulfoxide = nx.Graph()
sulfoxide.add_node("S", sybyl="S.O", ssipAtomType=None)
sulfoxide.add_node("O", sybyl="O.2", ssipAtomType="O.2.sulfoxide" )
sulfoxide.add_edge("S", "O" )

selenone = nx.Graph()
selenone.add_node("Se", sybyl="Se.O2", ssipAtomType=None)
selenone.add_node("O1", sybyl="O.2", ssipAtomType="O.2.selenone" )
selenone.add_node("O2", sybyl="O.2", ssipAtomType="O.2.selenone" )
selenone.add_edge("Se", "O1" )
selenone.add_edge("Se", "O2")

selenoxide = nx.Graph()
selenoxide.add_node("Se", sybyl="Se.O", ssipAtomType=None)
selenoxide.add_node("O", sybyl="O.2", ssipAtomType="O.2.selenoxide" )
selenoxide.add_edge("Se", "O" )

#NITROGEN

not_pyridine = nx.Graph()
not_pyridine.add_node("a1", ssipAtomType=None)
not_pyridine.add_node("N", sybyl="N.ar", ssipAtomType="N.ar.no_lp" )
not_pyridine.add_node("a2", ssipAtomType=None)
not_pyridine.add_node("a3", ssipAtomType=None)
not_pyridine.add_edge("a1", "N")
not_pyridine.add_edge("a2", "N")
not_pyridine.add_edge("a3", "N")

ammonia = nx.Graph()
ammonia.add_node("H1", elementType="H", ssipAtomType="H.N.ammonia")
ammonia.add_node("N", elementType="N", ssipAtomType="N.3.ammonia" )
ammonia.add_node("H2", elementType="H", ssipAtomType="H.N.ammonia")
ammonia.add_node("H3", elementType="H", ssipAtomType="H.N.ammonia")
ammonia.add_edge("H1", "N" , bondOrder=1)
ammonia.add_edge("H2", "N" , bondOrder=1)
ammonia.add_edge("H3", "N" , bondOrder=1)


primary_amine = nx.Graph()
primary_amine.add_node("H1", sybyl="H", ssipAtomType=None) #QUESTION: what should nitroamines be treated as?
primary_amine.add_node("N", sybyl="N.3", ssipAtomType="N.3.primary" )
primary_amine.add_node("H2", sybyl="H", ssipAtomType=None)
primary_amine.add_node("C3", sybyl="!H", ssipAtomType=None)
primary_amine.add_edge("H1", "N" , bondOrder=1)
primary_amine.add_edge("H2", "N" , bondOrder=1)
primary_amine.add_edge("C3", "N" , bondOrder=1)

secondary_amine = nx.Graph()
secondary_amine.add_node("H1", sybyl="H", ssipAtomType=None)
secondary_amine.add_node("N",  sybyl="N.3",  ssipAtomType="N.3.secondary" )
secondary_amine.add_node("A2", sybyl="!H", ssipAtomType=None)
secondary_amine.add_node("C3", sybyl="!H", ssipAtomType=None)
secondary_amine.add_edge("H1", "N" , bondOrder=1)
secondary_amine.add_edge("A2", "N" , bondOrder=1)
secondary_amine.add_edge("C3", "N" , bondOrder=1)

tertiary_amine = nx.Graph()
tertiary_amine.add_node("H1",  sybyl="!H", ssipAtomType=None)
tertiary_amine.add_node("N",  sybyl="N.3",  ssipAtomType="N.3.tertiary" )
tertiary_amine.add_node("A2", sybyl="!H", ssipAtomType=None)
tertiary_amine.add_node("A3",  sybyl="!H", ssipAtomType=None)
tertiary_amine.add_edge("H1", "N" , bondOrder=1)
tertiary_amine.add_edge("A2", "N" , bondOrder=1)
tertiary_amine.add_edge("A3", "N" , bondOrder=1)

primary_aniline = nx.Graph()
primary_aniline.add_node("H1", sybyl="H", ssipAtomType=None) #QUESTION: what should nitroamines be treated as?
primary_aniline.add_node("N", sybyl="N.pl3", ssipAtomType="N.pl3.primary" )
primary_aniline.add_node("H2", sybyl="H", ssipAtomType=None)
primary_aniline.add_node("C3", sybyl="!H", ssipAtomType=None)
primary_aniline.add_edge("H1", "N" , bondOrder=1)
primary_aniline.add_edge("H2", "N" , bondOrder=1)
primary_aniline.add_edge("C3", "N" , bondOrder=1)

secondary_aniline = nx.Graph()
secondary_aniline.add_node("H1", sybyl="H", ssipAtomType=None)
secondary_aniline.add_node("N",  sybyl="N.pl3",  ssipAtomType="N.pl3.secondary" )
secondary_aniline.add_node("A2", sybyl="!H", ssipAtomType=None)
secondary_aniline.add_node("C3", sybyl="!H", ssipAtomType=None)
secondary_aniline.add_edge("H1", "N" , bondOrder=1)
secondary_aniline.add_edge("A2", "N" , bondOrder=1)
secondary_aniline.add_edge("C3", "N" , bondOrder=1)

#This graph needs to be matched after the nitro
tertiary_aniline = nx.Graph()
tertiary_aniline.add_node("H1",  sybyl="!H", ssipAtomType=None)
tertiary_aniline.add_node("N",  sybyl="N.pl3",  ssipAtomType="N.pl3.tertiary" )
tertiary_aniline.add_node("A2", sybyl="!H", ssipAtomType=None)
tertiary_aniline.add_node("A3",  sybyl="!H", ssipAtomType=None)
tertiary_aniline.add_edge("H1", "N" , bondOrder=1)
tertiary_aniline.add_edge("A2", "N" , bondOrder=1)
tertiary_aniline.add_edge("A3", "N" , bondOrder=1)

#SULFUR AND SELENIUM
thiourea = nx.Graph()
thiourea.add_node("S", elementType="S", ssipAtomType="S.2.thiourea")
thiourea.add_node("C", elementType="C", ssipAtomType=None )
thiourea.add_node("N1", elementType="N", ssipAtomType=None)
thiourea.add_node("N2", elementType="N", ssipAtomType=None)
thiourea.add_edge("S", "C", bondOrder = 2 )
thiourea.add_edge("C", "N1", bondOrder = 1)
thiourea.add_edge("C", "N2", bondOrder = 1)

s_other = nx.Graph()
s_other.add_node("S", sybyl=["!S.2", "!S.3"], elementType = "S",  ssipAtomType="S")

se_other = nx.Graph()
se_other.add_node("S", sybyl=["!Se.2", "!Se.3"], elementType = "Se", ssipAtomType="Se")

